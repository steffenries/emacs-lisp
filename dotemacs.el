;;; dotemacs.el -- wrapper to load ~/emacs/lisp/*.org
;;; Commentary:
;;; wrapper to load literal Emacs config files
;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(defvar dotfiles-dir "~/emacs/lisp")
(require 'org-install nil t)
(require 'ob-tangle)
(mapc #'org-babel-load-file (directory-files dotfiles-dir t "\\.org$"))

;;; dotemacs.el ends here
