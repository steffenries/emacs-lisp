;;; emacs.el --- Steffen's emacs-defaults
;;;
;;; Commentary:
;;; emacs-defaults for site MORPORK
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Code:
(add-to-list 'load-path "~/emacs/lisp/")
(add-to-list 'load-path "~/emacs/lisp/psgml-1.4.0")

;;(setq debug-on-error t)

;; {{{ gnuserv
;;(require 'gnuserv)
;;(gnuserv-start)
;;(server-start)

(require 'mwheel)
(mwheel-install)

(require 'uniquify)


;; }}}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(when (require 'package nil t)
;;  (add-to-list 'package-archives
;;               '("melpa" . "https://melpa.org/packages/"))
  (add-to-list 'package-archives
               '("melpa-stable" . "http://stable.melpa.org/packages/") t)
;;  (add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
  (package-initialize))

;;;;;
(set-language-environment "Latin-1")
(transient-mark-mode t)

(defvar current-view "")
(when (file-exists-p "/homes/sries/bin/getsbname.py")
  (setq current-view (shell-command-to-string "/homes/sries/bin/getsbname.py")))
(if (> (length current-view) 0)
    (setq current-view (substring current-view 0 -1)))

(setq frame-title-format
      (if (member current-view '("" "None"
				 "/opt/tools/bin/p4client: Command not found."))
          '("" system-name ":%b")
        '("" current-view " " "%b")))
(setq icon-title-format '("%b"))

;;(setq p4-root (shell-command-to-string "/opt/tools/bin/p4getclient --root"))
;;(when (> (length p4-root) 0)
;;    (setq p4-root (substring p4-root 0 -1))
;;    (load-library "p4"))
(defvar p4-root)
(setq p4-root "")

;; }}}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; set mail address based on local file
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(let ((a (expand-file-name "~/.address")))
  (when (file-exists-p a)
    (setq user-mail-address (car (process-lines "cat" a)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {{{Enable ALL disabled commands!
;;    kids, don't do this at home. We are all trained professionals
;;    around here!
;;
(defun re-enable-magically ()
  "Re enable a disabled command automatically."
  (and (commandp this-command)
       (put this-command 'disabled nil))
  (call-interactively this-command))
(setq disabled-command-function 're-enable-magically)

;; answer `y' and `n' instead of `yes' and `no'
(fset 'yes-or-no-p 'y-or-n-p)
;;
;;(add-hook 'text-mode-hook '(lambda () (auto-fill-mode 1)))

;; }}}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {{{Customize C-Mode
(defconst my-c-style
  '("PERSONAL"
;;    (c-tab-always-indent           . nil)
    (c-comment-only-line-offset    . 0)
    (c-basic-offset                . 4)
    (comment-column                . 40)
    (c-electric-pound-behavior     . (alignleft))
    (c-hanging-braces-alist        . ((block-open after)
				      (brace-list-open)))
    (c-hanging-colons-alist        . ((member-init-intro before)
				      (inher-intro)
				      (case-label after)
				      (label after)
				      (access-key after)))
    (c-cleanup-list                . (scope-operator
				      empty-defun-braces
				      defun-close-semi))
    (c-offsets-alist               . ((arglist-close . c-lineup-arglist)
				      (case-label    . +)
				      (label         . 0)
				      (substatement-open . 0)
				      ))
    )
  "My C Programming Style.")

(defconst my-java-style
  '("java-personal"
    (c-basic-offset . 4)
    (c-comment-only-line-offset . (0 . 0))
    ;; the following preserves Javadoc starter lines
    (c-offsets-alist . ((inline-open . 0)
                        (topmost-intro-cont    . +)
                        (statement-block-intro . +)
                        (knr-argdecl-intro     . 5)
                        (substatement-open     . 0)
                        (label                 . +)
                        (statement-case-open   . +)
                        (statement-cont        . +)
                        (arglist-intro  . +)
                        (arglist-close  . c-lineup-arglist)
                        (access-label   . 0)
                        (inher-cont     . c-lineup-java-inher)
                        (func-decl-cont . c-lineup-java-throws)
                        ))

    )
  "My Java Programming Style.")

;; ;; Customizations for both c-mode and c++-mode
;; (defun my-c-mode-common-hook ()
;;   ;; set up for my perferred indentation style, but  only do it once
;;   (let ((my-style "PERSONAL"))
;;     (or (assoc my-style c-style-alist)
;; 	(setq c-style-alist (cons my-c-style c-style-alist)))
;;     (c-set-style my-style))
;;   ;; other customizations
;;   (if (fboundp 'c-enable-//-in-c-mode)
;;       (c-enable-//-in-c-mode))
;;   (setq tab-width 8
;; 	;; this will make sure spaces are used instead of tabs
;; 	indent-tabs-mode nil
;; 	c-recognize-knr-p nil))
;;
;;(require 'java-mode-indent-annotations)
;;
;;(defun my-java-mode-hook ()
;;  (let ((my-style "java-personal"))
;;    (or (assoc my-style c-style-alist)
;;        (setq c-style-alist (cons my-java-style c-style-alist)))
;;    (c-set-style my-style)
;;    (java-mode-indent-annotations-setup)))



;;(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)
(add-hook 'java-mode-hook 'my-java-mode-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(c-add-style "juniper"
	     '(
	       (c-basic-offset . 4)
	       (c-comment-only-line-offset . 0)
	       (c-offsets-alist
		. (
		   ;; first line of a new statement block
		   (statement-block-intro . +)

		   ;; First line of a K&R C argument declaration.
		   (knr-argdecl-intro . +)

		   ;; The brace that opens a substatement block.
		   (substatement-open . 0)

		   ;; Any non-special C label.
		   (label . -)

		   ;; A `case' or `default' label.
		   (case-label . +)

                   ;; c++ class access label
                   (access-label . /)

		   ;; The first line in a case block that starts with
		   ;; a brace.
		   (statement-case-open . +)

		   ;; A continuation of a statement.
		   (statement-cont . +)

		   ;; The first line after a conditional or loop
		   ;; construct.
		   (substatement . +)

		   ;; The first line in an argument list.
		   ;;(arglist-intro . c-lineup-arglist-intro-after-paren)
                   (arglist-intro . ++)

                   ;; (arglist-cont-nonempty . ++)

		   ;; The solo close paren of an argument list.
		   (arglist-close . c-lineup-arglist)

		   ;; Brace that opens an in-class inline method.
		   (inline-open . 0)

		   ;; Open brace of an enum or static array list.
		   (brace-list-open . 0)

                   ;; namespace does not create indendation
                   (innamespace . 0)))

	       (c-special-indent-hook . c-gnu-impose-minimum)
	       (c-block-comment-prefix . "")))

(defun juniper-c-default-style ()
  "Set the default c-style for Juniper."
  (interactive)
  ;;(define-key c-mode-map "\r" 'newline-and-indent)
  (define-key c-mode-base-map "\r" 'c-context-line-break)
  (subword-mode)
  (c-set-style "juniper"))

;;(require 'google-c-style)
;;(add-hook 'c-mode-common-hook 'google-set-c-style)
(add-hook 'c-mode-common-hook 'juniper-c-default-style)
;;(add-hook 'c-mode-common-hook  (lambda ()
;; (add-to-list 'write-file-functions 'delete-trailing-whitespace)))


(setq auto-mode-alist
      (append '(("\\.odl$"    . c-mode)
		("\\.dd$"     . c-mode)
		("\\.errmsg$" . c-mode)
                ("Makefile.inc" . makefile-mode))
	      auto-mode-alist))


;;; Emulate vi's ":set list" command.  Setting is buffer-local and
;;; won't screw up the rest of the editor session.  This probably
;;; should be a minor mode instead of two global functions.  Consider
;;; using the whitespace.el or show-whitespace-mode.el packages.

(defvar vi-list-display-table (make-display-table)
  "Vi-list display table for showing tabs and EOLs.")
(aset vi-list-display-table ?\t (vconcat "^I"))
(aset vi-list-display-table ?\n (vconcat "$\n"))

(defun vi-list ()
  "Simulate a :set list in Vi."
  (interactive)
  (setq buffer-display-table vi-list-display-table))

(defun vi-nolist ()
  "Simulate a :set nolist in Vi."
  (interactive)
  (setq buffer-display-table nil))


(defun my-c-mode-common-hook ()
  "Personal C mode."
   (let ((my-style "juniper"))
     (c-set-style my-style)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;(add-hook
;; 'java-mode-hook
;; (lambda () (c-set-style "java-personal")))
;;(setq c-offsets-alist
;;      (append '((substatement-open . 0))
;;              (assq-delete-all 'substatement-open c-offsets-alist)))))

;; End of C(++) mode customization
;; }}}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {{{python mode
(add-hook 'python-mode-hook (lambda () (setq indent-tabs-mode nil)))
;; }}}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {{{enable time
;(setq display-time-string-forms
;      '((format-time-string "%H:%M" now)
;	(if mail " Mail " "")))
(display-time)
;; }}}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {{{backup control
(setq version-control t			; use numbered backups
      kept-old-versions 0		; don't keep oldest versions
      kept-new-versions 5		; keep 5 newest versions
      delete-old-versions t)		; don't ask when deleting old ones
;; }}}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {{{insert-date
(defun insert-date ()
  "Insert current date in format 'DD-Mon-YY'."
  (interactive)
  (insert (format-time-string "%d-%b-%Y")))

(defun insert-date-login ()
  "Insert a mark containing the login name and the current date."
  (interactive)
  (insert "++" user-login-name "/")
  (insert-date))

(global-set-key [f11] 'insert-date-login)
(global-set-key [S-f11]   'insert-date)
;; }}}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(resize-minibuffer-mode 1)
(setq menu-bar-final-items '(buffer help-menu))
(setq highlight-nonselected-windows nil)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {{{
(defun my-info-other-frame ()
;; ************************************************************************
  "Private function: start *info* mode in another frame."
  (interactive)
  (let ((pop-up-frames t)
	(buffer (current-buffer)))
    (pop-to-buffer buffer t)
    (raise-frame (window-frame (selected-window)))
    (message "starting *info* in other frame ..." )
    (info)))
;; C-x 5 i	start info in other frame.
(global-set-key "\C-x5i" 'my-info-other-frame )
;;(load-library "iglimpse")
;; }}}
;; {{{desktop
;;(desktop-read)
(setq desktop-path '("~/.emacs.d/"))
(setq desktop-dirname "~/.emacs.d/")
(setq desktop-base-file-name "emacs-desktop")
;(setq desktop-files-not-to-save
;      (concat "\\("
;              "\\.log\\|(ftp)\\|^tags\\|^TAGS"
;              "\\|\\.emacs.*\\|\\|\\.diary"
;              "\\)$"))
;; remove desktop after it's been read
(add-hook 'desktop-after-read-hook
	  '(lambda ()
	     ;; desktop-remove clears desktop-dirname
	     (setq desktop-dirname-tmp desktop-dirname)
	     (desktop-remove)
	     (setq desktop-dirname desktop-dirname-tmp)))

(defun saved-session ()
  "Check if saved session exists."
  (file-exists-p (concat desktop-dirname "/" desktop-base-file-name)))

;; use session-restore to restore the desktop manually
(defun session-restore ()
  "Restore a saved Emacs session."
  (interactive)
  (if (saved-session)
      (desktop-read)
    (message "No desktop found.")))

;; use session-save to save the desktop manually
(defun session-save ()
  "Save an Emacs session."
  (interactive)
  (if (saved-session)
      (if (y-or-n-p "Overwrite existing desktop? ")
	  (desktop-save-in-desktop-dir)
	(message "Session not saved."))
  (desktop-save-in-desktop-dir)))

;; ask user whether to restore desktop at start-up
(add-hook 'after-init-hook
	  '(lambda ()
	     (if (saved-session)
		 (if (y-or-n-p "Restore desktop? ")
		     (session-restore)))))
;; }}}

;;(unload-feature 'xml-mode)

(autoload 'sgml-mode "psgml" "Major mode to edit SGML files." t)
(autoload 'xml-mode "psgml" "Major mode to edit XML files." t)
(autoload 'html-mode "xxml" "Major mode to edit HTML files." t)
(autoload 'xxml-mode-routine "xxml")
(add-hook 'sgml-mode-hook 'xxml-mode-routine)


(setq auto-mode-alist
      (cons '("\\.xml$" . xml-mode) auto-mode-alist))


;; XSL mode
(autoload 'xsl-mode "xslide" "Major mode for XSL stylesheets." t)

;; Uncomment if you want to use `xsl-grep' outside of XSL files.
;(autoload 'xsl-grep "xslide" "Grep for PATTERN in files matching FILESPEC." t)

;; Uncomment if you want to use `xslide-process' in `xml-mode'.
;(autoload 'xsl-process "xslide-process" "Process an XSL stylesheet." t)
;(add-hook 'xml-mode-hook
;	  (lambda ()
;	    (define-key xml-mode-map [(control c) (meta control p)]
;	      'xsl-process)))

;; Turn on font lock when in XSL mode
(add-hook 'xsl-mode-hook
	  'turn-on-font-lock)

(setq auto-mode-alist
      (append
       (list
	'("\\.fo" . xsl-mode)
	'("\\.xsl" . xsl-mode))
       auto-mode-alist))

;; Uncomment if using abbreviations
;(abbrev-mode t)

;;(add-to-list 'load-path "~/emacs/tramp/lisp/")
;;(require 'tramp)
;;(add-to-list 'tramp-remote-path "~/bin")
;;(setq tramp-default-method "ssh")

;; deprecated, use yasnippet instead!
;;(when (require 'tempo-c-cpp nil t))

(when (require 'mercurial nil t))

(defun workspace-path (list)
  "Prepend workspace path to elements of LIST."
  (reverse (let (value)
             (dolist (elt list value)
               (setq value (cons (concat p4-root elt) value))))))

;; '(jde-global-classpath (quote ("~/view/sae/classes" "~/view/sae/java" "~/view/pom/lib/pom.jar" "~/view/cops-pr/lib/copspr.jar" "~/view/util/classes" "~/view/ssportal/WEB-INF/classes" "~/view/ssportal/WEB-INF/src" "~/view/ssportal/lib/servlet.jar" "~/view/ssportal/WEB-INF/lib/struts.jar" "~/view/ssportal/WEB-INF/lib/gwnic.jar" "~/view/thirdParty/Beepcore/concurrent.jar")))
;; '(jde-sourcepath (quote ("~/view/sae/java" "~/view/sae/javaGenerated" "~/view/util/java" "~/view/telnetd" "~/view/ssp" "~/view/pom/java")))

;; FIXME: figure out if I'm currently editing a file in /volume/(wfd-junos-*).*
;; and set the compile command accordingly...
;;(setq compile-command
;;      (if (> (length p4-root) 0)
;;          (concat "cd " p4-root "/SRC; make -w")
;;        "ssh wfd-junos-d010 \"cd `pwd`; sdkmk\""))
;;"make -w")))

(setq compile-command
      (cond
       ((> (length p4-root) 0) (concat "cd " p4-root "/CCPE/ccpescp/ccpe/build; ant"))
       ((string-prefix-p "evosb:" current-view) "/homes/sries/bin/lmake")
       (t "make -w")))

;;(setq compile-command
;;      (cond ((> (length p4-root) 0)
;;             (concat "cd " p4-root "/CCPE/ccpescp/ccpe/build; ant"))
;;            ((string-prefix-p "/volume/wfd-junos-d010-b" buffer-file-name)
;;             (concat "ssh wfd-junos-d010 \"cd $PWD; mk\""))
;;            (t "make -w")))

;;(add-hook 'c-mode-hook
;;          (lambda ()
;;            (when (string-prefix-p "/volume/wfd-junos-d010-b" buffer-file-name)
;;              (set (make-local-variable 'compile-command)
;;                   "ssh wfd-junos-d010 \"cd $PWD; mk -DNO_DIRDEPS\""))))
;;

;;;;
(if (and (>= emacs-major-version 24) (>= emacs-minor-version 1))
    (delete-selection-mode 1)
  ;; older emacs version:
  (require 'pc-select)
  (pc-selection-mode))
;;
;;  {{{function keys
(global-set-key [delete] 'delete-char)
(global-set-key [S-backspace] 'backward-kill-word)
(global-set-key [?\C-m] 'newline-and-indent)
(global-set-key [?\C-j] 'newline)
;;
(global-set-key [f2]    'tags-search)           (global-set-key [S-f2]  nil)
;;(global-set-key [f3]    'search-forward)        (global-set-key [S-f3]  'search-backward)
(global-set-key [f3]    'kmacro-start-macro-or-insert-counter)        (global-set-key [S-f3]  'kmacro-end-macro)
(global-set-key [f4]    'next-error)            (global-set-key [S-f4]  'previous-error)
(global-set-key [f5]    'call-last-kbd-macro)   (global-set-key [S-f5]  nil)
;;(global-set-key [f6]    'occur)                 (global-set-key [S-f6]  nil)
;;(global-set-key [f7]    'ecb-activate)            (global-set-key [S-f7]  nil)

(global-set-key [f8]    'goto-line)             (global-set-key [S-f8]  'what-line)
;;(global-set-key [f9]    (lambda () (interactive) (set-buffer-file-coding-system 'undecided-unix)))
;;(global-set-key [S-f9]  (lambda () (interactive) (set-buffer-file-coding-system 'dos)))

(global-set-key [f10]   'compile)               ;;(global-set-key [S-f10] nil)
(global-set-key [f12]   'undo)
(global-set-key [S-f12] 'repeat-complex-command)

(global-set-key [rwindow] 'other-window)
(global-set-key [lwindow] 'other-window)

;; work-around for strange keybinding in CentOS7 dev-vm
;; keypad end looks like it was pressed with Shift
(global-set-key (kbd "<S-kp-end>") 'move-end-of-line)
;;  }}}
;;(global-set-key "\C-x\C-b" 'electric-buffer-list)
;; }}}

(global-set-key "%" 'match-paren)

;; enable when needed for Java development...
;;(if (< emacs-major-version 23)
;;    nil
;;  (add-to-list 'load-path "~/emacs/jdee-2.4.1/lisp")
;;  (require 'jde))

(defun match-paren (arg)
  "Go to the matching parenthesis if on parenthesis otherwise insert % ARG times."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
	((looking-at "\\s\)") (forward-char 1) (backward-list 1))
	(t (self-insert-command (or arg 1)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;
;(add-to-list 'load-path "~/emacs/guess-style")
;(autoload 'guess-style-set-variable "guess-style" nil t)
;(autoload 'guess-style-guess-variable "guess-style")
;(autoload 'guess-style-guess-all "guess-style" nil t)

;;(add-hook 'c-mode-common-hook 'guess-style-guess-all)
;(add-hook 'js-mode-hook 'guess-style-guess-all)
;(add-hook 'java-mode-hook 'guess-style-guess-all)
;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun grand-parent-of-file (file)
  "Return path of grand-parent directory of FILE.
e.g. if FILE is /a/b/c/d, return /a/b"
  (mapconcat 'identity (butlast (split-string file "/") 2) "/"))

(setq custom-file "~/.emacs-custom.el")
(load custom-file)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; javascript environment
;; http://blog.deadpansincerity.com/2011/05/setting-up-emacs-as-a-javascript-editing-environment-for-fun-and-profit/

;;(add-to-list 'load-path "~/emacs/auto-complete-1.3.1")
;;(require 'auto-complete-config)
;;(add-to-list 'ac-dictionary-directories "~/emacs/auto-complete-1.3.1/dict")
;;(setq-default ac-sources (add-to-list 'ac-sources 'ac-source-dictionary))
;;(global-auto-complete-mode t)
;;(setq ac-auto-start 2)
;;(setq ac-ignore-case nil)

;;(require 'flymake-jslint)
;;(add-hook 'js-mode-hook 'flymake-jslint-load)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; enable hide/show minor mode
(when (require 'fold-dwim nil t)
  (global-set-key (kbd "<print>")      'fold-dwim-toggle)
  ;;(global-set-key (kbd "<M-f7>")    'fold-dwim-hide-all)
  ;;(global-set-key (kbd "<S-M-f7>")  'fold-dwim-show-all)
  )
;;;
;;;(autoload 'hideshowvis-enable "hideshowvis" "Highlight foldable regions")
;;;
;;;(autoload 'hideshowvis-minor-mode
;;;  "hideshowvis"
;;;  "Will indicate regions foldable with hideshow in the fringe."
;;;  'interactive)
;;;
;;;(dolist (hook (list 'emacs-lisp-mode-hook
;;;                    'c-mode-common-hook
;;;                    'java-mode-hook
;;;                    'lisp-mode-hook
;;;                    'perl-mode-hook
;;;                    'sh-mode-hook))
;;;  (add-hook hook 'hideshowvis-minor-mode))
;;;

(add-hook 'c-mode-common-hook   'hs-minor-mode)
(add-hook 'emacs-lisp-mode-hook 'hs-minor-mode)
(add-hook 'java-mode-hook       'hs-minor-mode)
(add-hook 'lisp-mode-hook       'hs-minor-mode)
(add-hook 'perl-mode-hook       'hs-minor-mode)
(add-hook 'sh-mode-hook         'hs-minor-mode)

(defadvice goto-line (after expand-after-goto-line
                            activate compile)
  "Hideshow-expand affected block when using `goto-line' in a collapsed buffer."
  (save-excursion
    (hs-show-block)))

(setq org-default-notes-file "~/notes/notes.org")
;;(define-key global-map "\C-cc" 'org-capture)
;;(setq org-link-abbrev-alist
;;       '(("rfc" . "https://tools.ietf.org/html/rfc")
;;         ("rli" . "https://deepthought.juniper.net/app/do/showView?tableName=RLI&taskCode=all&record_number=")
;;         ("google" . "http://www.google.com/search?q=")
;;         ("gmap" . "http://maps.google.com/maps?q=%s")))

(setq org-publish-project-alist
      '(("notes"
         :base-directory "~/notes/"
         :section-numbers nil
         :table-of-contents nil)))

(when (require 'org-mouse nil t))

(when (executable-find "ispell")
  ;; turn on spell checker for comments
  (add-hook 'c-mode-common-hook 'flyspell-prog-mode)
  (add-hook 'python-mode-hook 'flyspell-prog-mode)

  (add-hook 'org-mode-hook 'flyspell-mode))

;; set keys for debugging
(global-set-key (kbd "<pause>") 'gud-break)
(global-set-key (kbd "C-<break>") 'gud-remove)
(global-set-key [f6] 'gud-next)
(global-set-key [S-f6] 'gud-cont)
(global-set-key [C-f6] 'gud-print)
(global-set-key [f7]   'gud-step)
(global-set-key [C-f7] 'gud-finish)

;; convenience function to start gdb
(defun switch-to-gud-buffer ()
  "Search for an active GUD buffer and switch to it."
  (let ((gud-buffers (delq nil
                           (mapcar (lambda (buf)
                                     (if (string-prefix-p "*gud-" (buffer-name buf)) buf))
                                   (buffer-list)))))
    (if gud-buffers (switch-to-buffer (car gud-buffers)))))
(defun my-start-gdb ()
  "Start gdb in the existing buffer."
  (interactive)
  (switch-to-gud-buffer)
  (if (and (boundp 'gud-comint-buffer)
           gud-comint-buffer (get-buffer-process gud-comint-buffer))
      (gdb-restore-windows)
    (call-interactively 'gdb)))
(global-set-key [C-f10] #'my-start-gdb)

(defun toggle-window-dedicated ()
  "Toggle whether the current active window is dedicated or not"
  (interactive)
  (message (if (let (window (get-buffer-window (current-buffer)))
                 (set-window-dedicated-p window (not (window-dedicated-p window))))
               "Window '%s' is dedicated"
             "Window '%s' is normal")
           (current-buffer)))

(defun gtags-root-dir ()
  "Return GTAGS root directory or nil if doesn't exist."
  (car (ignore-errors (process-lines "global" "-pr"))))

(defun gtags-update ()
  "Make GTAGS incremental update."
  (let ((root-dir (gtags-root-dir)))
    (when root-dir
        (call-process "update-gtags" nil 0 nil))))

(add-hook 'after-save-hook
          '(lambda ()
             (if (getenv "SB")
                 (call-process "evo-gtags" nil 0 nil)
               (gtags-update))))

;;; WIP: change sr-speedbar to open files relative to project-root
(when nil (require 'project-root nil t)
  (setq project-roots
        `(("sandbox"
           :root-contains-files (".repo" ".emake"))
          ))
  
  (defun nm-speedbar-expand-line-list (&optional arg)
    (when arg
      (message (car arg))
      (re-search-forward (concat " " (car arg) "$"))
      (speedbar-expand-line (car arg))
      (speedbar-next 1) ;; Move into the list.
      (nm-speedbar-expand-line-list (cdr arg))))

  (defun get-root-dir ()
    (let ((sb ((get symbol {2:propname})env "SB")))
      (if (not sb)
          default-directory
        (if (string-prefix-p sb default-directory)
            sb default-directory))))

  (defun nm-speedbar-open-current-buffer-in-tree ()
    (interactive)
    (let* ((root-dir (cdr (project-root-fetch)))
           (original-buffer-file-directory (file-name-directory (buffer-file-name)))
           (relative-buffer-path (car (cdr (split-string original-buffer-file-directory root-dir))))
           (parents (butlast (split-string relative-buffer-path "/"))))
      (save-excursion 
        (sr-speedbar-open) ;; <--- or whatever speedbar you have e.g. (speedbar 1)
        (set-buffer speedbar-buffer)
        (beginning-of-buffer)
        (nm-speedbar-expand-line-list parents)))))

;; Cycle between snake case, camel case, etc.
(when (require 'string-inflection nil t)
  (global-set-key (kbd "C-c i") 'string-inflection-cycle)
  (global-set-key (kbd "C-c C") 'string-inflection-camelcase)        ;; Force to CamelCase
  (global-set-key (kbd "C-c L") 'string-inflection-lower-camelcase)  ;; Force to lowerCamelCase
  (global-set-key (kbd "C-c J") 'string-inflection-java-style-cycle) ;; Cycle through Java styles
  (global-set-key (kbd "C-c U") 'string-inflection-underscore)
  )
(winner-mode)

(defun my-transpose-sexps ()
  "If point is after certain chars transpose chunks around that.
Otherwise transpose sexps."
  (interactive "*")
  (if (not (looking-back "[,]\\s-*" (point-at-bol)))
      (progn (transpose-sexps 1) (forward-sexp -1))
    (let ((beg (point)) end rhs lhs)
      (while (and (not (eobp))
                  (not (looking-at "\\s-*\\([,]\\|\\s)\\)")))
        (forward-sexp 1))
      (setq rhs (buffer-substring beg (point)))
      (delete-region beg (point))
      (re-search-backward "[,]\\s-*" nil t)
      (setq beg (point))
      (while (and (not (bobp))
                  (not (looking-back "\\([,]\\|\\s(\\)\\s-*" (point-at-bol))))
        (forward-sexp -1))
      (setq lhs (buffer-substring beg (point)))
      (delete-region beg (point))
      (insert rhs)
      (re-search-forward "[,]\\s-*" nil t)
      (save-excursion
        (insert lhs)))))
(global-set-key (kbd "C-c T") 'my-transpose-sexps)


(defun int-to-hex-at-point ()
  "Format the decimal int at point to 4 digit hex."
  (interactive)
  (let ((i (thing-at-point 'word))
        (b (beginning-of-thing 'word))
        (e (end-of-thing 'word)))
    (delete-region b e)
    (insert (format "%04x" (string-to-number i)))))

(defun hex-to-int-at-point ()
  "Convert hex integer at point to decimal."
  (interactive)
  (let ((i (thing-at-point 'word))
        (b (beginning-of-thing 'word))
        (e (end-of-thing 'word)))
    (delete-region b e)
    (insert (format (concat "%" (int-to-string (- e b)) "d") (string-to-number i 16)))))
(global-set-key (kbd "C-c H") 'hex-to-int-at-point)


;;; experimental change to compile command
;;

;; I'm not scared of saving everything.
(setq compilation-ask-about-save nil)
;; Stop on the first error.
(setq compilation-scroll-output 'first-error)
;; Don't stop on info or warnings.
(setq compilation-skip-threshold 2)

(defcustom endless/compile-window-size 105
    "Width given to the non-compilation window."
      :type 'integer
        :group 'endless)

(defun endless/compile-please (comint)
  "Compile without confirmation.
With a prefix argument COMINT, use `comint-mode'."
  (interactive "P")
  ;; Do the command without a prompt.
  (save-window-excursion
    (compile (eval compile-command) (and comint t)))
  (pop-to-buffer (get-buffer "*compilation*")))
;;  ;; Create a compile window of the desired width.
;;  (pop-to-buffer (get-buffer "*compilation*"))
;;  (enlarge-window
;;   (- (frame-width)
;;      endless/compile-window-size
;;      (window-width))
;;   'horizontal))

;;(setq compilation-window-height 10)
;;(defun my-compilation-hook ()
;;  (when (not (get-buffer-window "*compilation*"))
;;    (save-selected-window
;;      (save-excursion
;;        (let* ((w (split-window-vertically))
;;               (h (window-height w)))
;;          (select-window w)
;;          (switch-to-buffer "*compilation*")
;;          (shrink-window (- h compilation-window-height)))))))
;;(add-hook 'compilation-mode-hook 'my-compilation-hook)


;; This gives a regular `compile-command' prompt.
(global-set-key [S-f10] 'compile)
;; This just compiles immediately.
(global-set-key [f10] 'endless/compile-please)


;; configure semantic for IDE
(when nil
  (when (require 'semantic nil t)
    (global-semantic-idle-scheduler-mode)
    (global-semantic-idle-completions-mode)
    (global-semantic-decoration-mode)
    (global-semantic-highlight-func-mode)
    (semanticdb-enable-gnu-global-databases 'c-mode)
    (semanticdb-enable-gnu-global-databases 'c++-mode)

    (defun evo-add-includes ()
      "Guess the include paths for the current project and update semanticdb."
      (interactive)
      (mapc 'semantic-add-system-include
            (ignore-errors (process-lines "get-include-paths"))))

    (defun evo-remove-includes ()
      "Guess the include paths for the current project and update semanticdb."
      (interactive)
      (mapc 'semantic-remove-system-include
            (ignore-errors (process-lines "get-include-paths"))))

    (add-hook 'c-mode-common-hook
              '(lambda ()
                 (semantic-mode 1)
                 (semantic-add-system-include "../include"))))


  (when (require 'auto-complete-config nil t)
    (add-to-list 'ac-dictionary-directories
                 (expand-file-name
                  (car (file-expand-wildcards
                        "~/.emacs.d/elpa/auto-complete-*/dict"))))
    (setq ac-comphist-file (expand-file-name "~/.emacs.d/ac-comphist.dat"))
    (ac-config-default)
    (add-hook 'c-mode-common-hook
              '(lambda ()
                 (add-to-list 'ac-sources 'ac-source-gtags)
                 (add-to-list 'ac-sources 'ac-source-semantic))))
  )

(when nil ;; enable srefactor
  (require 'srefactor nil t)
  (require 'srefactor-lisp)
  (semantic-mode 1)
  (define-key c-mode-map (kbd "M-RET") 'srefactor-refactor-at-point)
  (define-key c++-mode-map (kbd "M-RET") 'srefactor-refactor-at-point)
  (global-set-key (kbd "M-RET o") 'srefactor-lisp-one-line)
  (global-set-key (kbd "M-RET m") 'srefactor-lisp-format-sexp)
  (global-set-key (kbd "M-RET d") 'srefactor-lisp-format-defun)
  (global-set-key (kbd "M-RET b") 'srefactor-lisp-format-buffer))

(when nil ;; enable irony-mode
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)

  ;; replace the `completion-at-point' and `complete-symbol' bindings in
  ;; irony-mode's buffers by irony-mode's asynchronous function
  (defun my-irony-mode-hook ()
    (define-key irony-mode-map [remap completion-at-point]
      'irony-completion-at-point-async)
    (define-key irony-mode-map [remap complete-symbol]
      'irony-completion-at-point-async))
  (add-hook 'irony-mode-hook 'my-irony-mode-hook)

  ;; Only needed on Windows
  (when (eq system-type 'windows-nt)
    (setq w32-pipe-read-delay 0))

  (eval-after-load 'flycheck
    '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup)))

(when (require 'flycheck nil t) ;; enable flycheck-mode
  (global-flycheck-mode)
  (global-set-key [C-f4]    'flycheck-next-error)
  (add-hook
   'c++-mode-hook
   #'(lambda ()
       (setq flycheck-gcc-language-standard "c++14")
       (setq flycheck-clang-language-standard flycheck-gcc-language-standard)))

  (when (require 'flycheck-google-cpplint nil t)
    (flycheck-add-next-checker 'c/c++-cppcheck
                               '(warning . c/c++-googlelint)))

  (defun flycheck-add-evo-includes ()
    "Guess the include paths for the current project and update flycheck."
    (interactive)
    (when (getenv "SB") ;; only setup options if inside EVO sandbox
      (setq-local flycheck-c/c++-gcc-executable
                  (car (ignore-errors (process-lines "get-poky-base" "gxx"))))
      (setq-local flycheck-gcc-definitions
                  (ignore-errors (process-lines "get-include-paths"
                                                "--action=define")))
      (setq-local flycheck-gcc-include-path
                  (ignore-errors (process-lines "get-include-paths")))
      (setq-local flycheck-clang-definitions
                  (cons "YOCTO" flycheck-gcc-definitions))
      (setq-local flycheck-clang-include-path flycheck-gcc-include-path)
      (setq-local flycheck-googlelint-root
                  (car (ignore-errors (process-lines "getRoot.py"))))
      (setq-local company-clang-arguments
                  (append '("-std=c++14")
                          (ignore-errors (process-lines "get-include-paths"
                                                        "-r"))))))
  (add-hook 'c-mode-common-hook #'flycheck-add-evo-includes))
;;; end flycheck-mode

(when (string-prefix-p "evosb:" current-view)
  ;; inside evo sandbox
  (defun c-c++-header ()
    "Sets either c-mode or c++-mode based on best guess"
    (interactive)
    (let ((c-file (concat (substring (buffer-file-name) 0 -1) "c")))
      (if (file-exists-p c-file)
          (c-mode)
        (c++-mode))))

  (add-to-list 'auto-mode-alist '("\\.h\\'" . c-c++-header))

  (defun c-c++-toggle ()
    "toggles between c-mode and c++-mode"
    (interactive)
    (cond ((string= major-mode "c-mode") (c++-mode))
          ((string= major-mode "c++-mode") (c-mode))))
  (global-set-key (kbd "C-C m") 'c-c++-toggle))

(when (require 'iedit nil t)
  (global-set-key (kbd "C-;") 'iedit-mode))

(windmove-default-keybindings 'meta)

(require 'whitespace)
(setq whitespace-style '(face empty tabs lines-tail trailing))
(setq whitespace-global-modes '(c-mode c++-mode python-mode go-mode))
(global-whitespace-mode 1)

(defun insert-static-prototypes ()
  "Insert prototypes for static functions defined in the current file."
  (interactive)
  (call-process "getStatic.py" nil t nil (buffer-file-name)))

(prefer-coding-system 'utf-8)

(global-set-key (kbd "C-x v /") 'ediff-revision)

;;(when (require 'theme-changer nil t)
;;  (setq calendar-location-name "Ottawa, ON"
;;        calendar-latitude 45.42
;;        calendar-longitude -75.68)
;;  (change-theme 'adwaita 'deeper-blue))

;;(when (require 'ido nil t)
;;  (ido-mode t)
;;  (setq ido-enable-flex-matching t))
;;(when (require 'smex nil t)
;;  (smex-initialize)
;;  (global-set-key (kbd "M-x") 'smex)
;;  (global-set-key (kbd "M-X") 'smex-major-mode-commands)
;;  (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command))


(when (require 'ycmd nil t)
  (add-hook 'after-init-hook #'global-ycmd-mode)
  (set-variable 'ycmd-server-command `("python" ,(file-truename "~/.emacs.d/ycmd/ycmd")))
  ;; (set-variable 'ycmd-global-config "/homes/sries/src/ycmd/extra_conf.py")
  (when (require 'company-ycmd nil t)
    ;;(company-ycmd-setup)
    (setq company-idle-delay 0.5)
    (defun company-ycmd-c-hook ()
      (set-variable 'company-backends '((company-ycmd :with company-yasnippet)) t))
    (add-hook 'c-mode-common-hook #'company-ycmd-c-hook)
    (push '(company-ycmd :with company-yasnippet) company-backends)))

;;(when (require 'sr-speedbar nil t)
;;  (global-set-key (kbd "s-s") 'sr-speedbar-toggle)
;;  (setq sr-speedbar-width-x 15
;;        sr-speedbar-right-side nil))


(setq dired-dwim-target t)

(defalias 'ag 'helm-ag)

;; global replace HTML special characters with entity
(defun unhtml (start end)
  "Replace reserved characters with HTML entity in region START to END."
  (interactive "r")
  (save-excursion
    (replace-string "&" "&amp;" nil start end)
    (replace-string "<" "&lt;" nil start end)
    (replace-string ">" "&gt;" nil start end)))

(when (require 'member-functions nil t)
  (add-hook 'c++-mode-hook (lambda () (local-set-key "\C-cm" #'expand-member-functions))))

(when (require 'use-package nil t)
  (use-package company
    :init (progn (add-hook 'after-init-hook #'global-company-mode)
                 (set-variable 'company-dabbrev-downcase nil)))
  (use-package helm
    :init (progn (require 'helm-config nil t)
                 (global-unset-key (kbd "C-x c"))
                 (when (executable-find "curl")
                   (setq helm-google-suggest-use-curl-p t))
                 (setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
                       helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
                       helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
                       helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
                       helm-ff-file-name-history-use-recentf t
                       helm-echo-input-in-header-line t)
                 (helm-mode 1))

    :bind (("C-c C-h" . helm-comand-prefix)
           ("C-x C-f" . helm-find-files)
           ("M-x" . helm-M-x)
           ("<f9>" . helm-resume)
           :map helm-map
           ("<tab>" . helm-execute-persistent-action)
           ("C-i" . helm-execute-persistent-action)
           ("C-z" . helm-select-action)))

  (use-package helm-gtags
    :init (progn
            (add-hook 'c-mode-hook 'helm-gtags-mode)
            (add-hook 'c++-mode-hook 'helm-gtags-mode)
            (add-hook 'asm-mode-hook 'helm-gtags-mode)
            (add-hook 'python-mode-hook 'helm-gtags-mode)
            (add-hook 'go-mode-hook 'helm-gtags-mode))
    :bind (:map helm-gtags-mode-map
                ("M-t"    . helm-gtags-find-tag)
                ("M-r"    . helm-gtags-find-rtag)
                ("M-s"    . helm-gtags-find-symbol)
                ("C-c <"  . helm-gtags-previous-history)
                ("C-c >"  . helm-gtags-next-history)
                ("M-,"    . helm-gtags-pop-stack)
                ("M-."    . helm-gtags-find-tag)
                ("<f2>"   . helm-gtags-find-rtag)
                ("S-<f2>" . helm-gtags-find-tag-from-here)
                ("C-<f2>" . helm-gtags-find-files)
                ("M-*"    . helm-gtags-pop-stack)))
    
  (use-package yasnippet
    :config (progn (setq yas-snippet-dirs '("~/emacs/lisp/snippets"))
                   (if (require 'helm-c-yasnippet nil t)
                       (define-key yas-minor-mode-map (kbd "C-<return>") 'helm-yas-complete)
                     (define-key yas-minor-mode-map (kbd "C-<return>") 'yas-expand))
                   (yas-global-mode 1)))
  (use-package simpleclip
    :config (simpleclip-mode 1))
  (use-package simplenote2
    :config (progn
              (setq simplenote2-email "steffen.ries@gmail.com"
                    simplenote2-password "C3ZwrhLs4zemNpx4LCEr"
                    simplenote2-notes-mode 'org-mode)
              (simplenote2-setup)))
  (use-package ansible-doc
    :init (add-hook 'yaml-mode-hook #'ansible-doc-mode))
  (use-package go-dlv)
  (use-package treemacs
    :ensure t
    :defer t
    :config (progn
              (setq treemacs-collapse-dirs (if (executable-find "python") 3 0)
                    treemacs-deferred-git-apply-delay 0.5
                    treemacs-display-in-side-window   t
                    treemacs-width                    35))
    :bind (("s-t" . treemacs)))
  )

(when (require 'shackle nil t)
  (setq shackle-rules '((compilation-mode :noselect t))
        shackle-default-rule '(:select t)))

;;; .emacs.el ends here
