(require 'yasnippet)

(defun yas-c++-class-name (str)
  "Search for a class name like `DerivedClass' in STR
(which may look like `DerivedClass : ParentClass1, ParentClass2, ...')

If found, the class name is returned, otherwise STR is returned"
  (yas-substr str "[^: ]*"))

(defun yas-c++-class-method-declare-choice ()
  "Choose and return the end of a C++11 class method declaration"
  (yas-choose-value '(";" " = default;" " = delete;")))

(defun c++/get-class-name ()
  "Guess the name of the C++ class name. Point needs to be inside a class
definition."
  (save-excursion
    (beginning-of-line)
    (c-beginning-of-defun)
    (if (looking-at "class")
        (buffer-substring (progn (forward-sexp 2) (point))
                          (progn (backward-sexp) (point)))
      "Class")))
