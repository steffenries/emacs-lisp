;; Collection of simple keyboard macros

;; change "virtual void foo();" to "void foo() override"
(fset 'virtual-to-override
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([19 118 105 114 116 117 97 108 return right S-home delete tab 19 59 return left 32 111 118 101 114 114 105 100 101] 0 "%d")) arg)))

;; change "type value;" to "type GetValue() const { return value; }
(fset 'make-getter
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([67108896 134217734 23 19 59 return backspace 123 125 44 right] 0 "%d")) arg)))

;; change "typedef foo bar;" to "using bar = foo;"
(fset 'typedef-to-using
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([19 116 121 112 101 100 101 102 return C-S-left 117 115 105 110 103 19 59 return left 67108896 134217730 23 18 117 115 105 110 103 return 134217734 32 25 32 61 32 escape 32] 0 "%d")) arg)))
